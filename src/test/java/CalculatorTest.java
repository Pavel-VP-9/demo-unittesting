import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {
    @Test
    public void testSum1() {
        Calculator cal = new Calculator();
        int result = cal.sum(5, 6);
        Assert.assertEquals(11, result);
    }

    @Test
    public void testSum2() {
        Calculator cal = new Calculator();
        int result1 = cal.sum(11, 12);
        Assert.assertEquals(20, result1);
    }

    @Test
    public void testSum3() {
        Calculator cal = new Calculator();
        int result2 = cal.sum(16, 17);
        Assert.assertEquals(30, result2);
    }

    @Test
    public void testSum4() {
        Calculator cal = new Calculator();
        int result3 = cal.sum(21, 22);
        Assert.assertEquals(40, result3);
    }

    @Test
    public void testSum5() {
        Calculator cal = new Calculator();
        int result4 = cal.sum(5, -5);
        Assert.assertEquals(0, result4);

    }

    @Test
    public void TestSubtract1() {
        Calculator cal = new Calculator();
        int result = cal.subtract(7, 5);
        Assert.assertEquals(2, result);
    }

    @Test
    public void TestSubtract2() {
        Calculator cal = new Calculator();
        int result1 = cal.subtract(6, -5);
        Assert.assertEquals(6, result1);
    }

    @Test
    public void TestSubtract3() {
        Calculator cal = new Calculator();
        int result2 = cal.subtract(1, 3);
        Assert.assertEquals(-3, result2);

    }

    @Test
    public void TestMultiply1() {
        Calculator cal = new Calculator();
        int result = cal.multiply(1, -1);
        Assert.assertEquals(-1, result);
    }

    @Test
    public void TestMultiply2() {
        Calculator cal = new Calculator();
        int result1 = cal.multiply(5, 5);
        Assert.assertEquals(25, result1);
    }

    @Test
    public void TestMultiply3() {
        Calculator cal = new Calculator();
        int result2 = cal.multiply(20, 20);
        Assert.assertEquals(100, result2);
    }

    @Test
    public void TestMultiply4() {
        Calculator cal = new Calculator();
        int result3 = cal.multiply(40, 40);
        Assert.assertEquals(1000, result3);
    }

    @Test
    public void TestMultiply5() {
        Calculator cal = new Calculator();
        int result4 = cal.multiply(120, 140);
        Assert.assertEquals(10000, result4);

    }

    @Test
    public void TestDivision1() {
        Calculator cal = new Calculator();
        int result = cal.divide(0, 0);
        Assert.assertEquals(0, result);
    }

    @Test
    public void TestDivision2() {
        Calculator cal = new Calculator();
        int result1 = cal.divide(4, 4);
        Assert.assertEquals(1, result1);
    }

    @Test
    public void TestDivision3() {
        Calculator cal = new Calculator();
        int result2 = cal.divide(2, 3);
        Assert.assertEquals(1, result2);


    }
}
