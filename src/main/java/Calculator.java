
public class Calculator {

    public int sum(int a, int b) {
        int result=0;
        if (0 <= a && a < 10 && 0 <= b && b < 10) {
            result = a + b;

        } else if (10 <= a && a < 20 && 10 <= b && b < 20 && 20 <= (a + b) && (a + b) < 30) {
            result = 20;

        } else if (10 <= a && a < 20 && 10 <= b && b < 20 && 30 <= (a + b) && (a + b) < 40) {
            result = 30;

        } else if (a >= 20 && b >= 20) {
            result = 40;

        } else if (a < 0 || b < 0) {
            result = 0;

        } else {
            result = 111111111;
        }
        return result;
    }

    public int subtract(int a, int b) {
        int result=0;
        if (a >= b && a >= 0 && b >= 0) {
            result = a - b;
        } else if (a >= b && a >= 0 && b < 0) {
            result = a;
        } else if (a < b) {
            result = -b;
        } else {
            result=2222222;
        }
        return result;
    }
    public int multiply(int a, int b) {
        int result=0;
        if (a < 0 || b < 0) {
            result = -1;
        } else if (0 <= a && a < 10 && 0 <= b && b < 10) {
            result = a * b;
        } else if (10 <= a && a < 100 && 10 <= b && b < 100 && 100 <= (a * b) && (a * b) < 1000) {
            result = 100;
        } else if (10 <= a && a < 100 && 10 <= b && b < 100 && 1000 <= (a * b) && (a * b) < 10000) {
            result = 1000;
        } else if (a >= 100 || b >= 100) {
            result = 10000;
        } else {
            result=333333;
        }
        return result;
    }
    public int divide(int a, int b){
        int result=0;
        if( b<=0){
            result= 0;
        } else if (a>=b) {
            result = a / b;
        }
        else if( a<b) {
            result = 1;
        } else{
            result = 44444;
        }
        return result;
    }
}
